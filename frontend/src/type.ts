export type Location = {
  categoryId: number;
  description: string;
  id: number;
  location: string;
  numberOfRooms: number;
  picture: string;
  price: number;
  starts: number;
  title: string;
};

export type Category = {
  id: number;
  description: string;
  propertyType: string;
};
