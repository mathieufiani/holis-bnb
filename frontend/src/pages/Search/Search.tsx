import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './Search.css';
import Card from '../../components/Card/Card';
import { Location, Category } from '../../type';
import { useSelector } from 'react-redux';
import Loader from '../../components/Loader/Loader';

type SearchPageProps = {};

type SortedLocation = {
  [key: string]: SortedCategory;
};

type SortedCategory = {
  [key: number]: Room;
};

type Room = {
  [key: number]: Location[];
};

type State = {
  searchName: SearchName;
};

type SearchName = {
  searchName: string;
};

const SearchPage: React.FC<SearchPageProps> = () => {
  // Create a function to fetch all locations from database
  const [locations, setLocations] = useState<Location[] | []>([]);
  const [categories, setCategories] = useState<Category[] | []>([]);
  // eslint-disable-next-line no-unused-vars
  const [sortedLocations, setSortedLocations] = useState<SortedLocation | object>({});
  const [searchedLocations, setSearchedLocation] = useState<Location[] | []>([]);
  // Use redux to get searchName from search bar.
  const { searchName } = useSelector((state: State) => state.searchName);

  const loadData = async () => {
    await axios.get('http://localhost:8000/categories').then((response: any) => {
      setCategories(response.data);
    });
    await axios.get('http://localhost:8000/locations').then((response: any) => {
      setLocations(response.data);
      setSearchedLocation(response.data);
    });
  };

  /**
   * Loading data on page init.
   */
  useEffect(() => {
    loadData();
  }, []);

  /**
   * Sort on filtered Locations from search feature or locations
   */
  useEffect(() => {
    if (locations.length > 0 && searchedLocations.length === 0) {
      sortLocations(locations);
    } else if (searchedLocations.length > 0) {
      sortLocations(searchedLocations);
    }
  }, [locations, categories, searchedLocations]);

  useEffect(() => {
    searchLocations();
  }, [searchName]);

  /**
   * search locations that match the keyword entered in the search bar.
   */
  const searchLocations = () => {
    if (searchName) {
      let locationsSelected: Location[] | [] = locations.filter(
        (location: Location) =>
          location.title.toLowerCase().includes(searchName) ||
          location.description.toLowerCase().includes(searchName)
      );
      return setSearchedLocation(locationsSelected);
    }
    return setSearchedLocation(locations);
  };

  /**
   * Sort Array of locations into an object where locations are sorted into categories and then number of rooms
   * @param locations
   */
  const sortLocations = (locations: Location[]) => {
    setSortedLocations(
      // @ts-ignore
      categories.reduce((acc: any, category: Category) => {
        const locationCat = locations.filter((el: Location) => el.categoryId === category.id);
        const locRooms = locationCat.reduce((acc2: any, loc: Location) => {
          return {
            ...acc2,
            [loc.numberOfRooms]: locationCat.filter((el) => el.numberOfRooms === loc.numberOfRooms)
          };
        }, {});
        return { ...acc, [category.propertyType]: locRooms };
      }, {})
    );
  };

  return (
    <div className="search">
      <h1>Look at our awesome homes for your stay !</h1>
      <div>
        {Object.keys(sortedLocations).length > 0 ? (
          // map over first layer : the categories
          Object.keys(sortedLocations).map((location: string) => {
            // @ts-ignore
            const actualSortedLocationClass = sortedLocations[location];
            return (
              <div key={location}>
                <h2>{location && location.charAt(0).toUpperCase() + location.slice(1)}s</h2>
                {/*map over the second layer, the number of rooms in each categories*/}
                {Object.keys(actualSortedLocationClass).map((room: string, index: number) => {
                  const properties: Location[] = actualSortedLocationClass[parseInt(room)];
                  return (
                    <div key={index}>
                      <h3 className={'search__room-text'}>{room} Rooms</h3>
                      <div className={'search__properties'}>
                        {/*map over the array of locations that have same number of rooms and category*/}
                        {properties.map((property: Location, index: number) => (
                          <Card location={property} key={index} />
                        ))}
                      </div>
                    </div>
                  );
                })}
              </div>
            );
          })
        ) : (
          <div className={'search__loader'}>
            <Loader />
          </div>
        )}
      </div>
    </div>
  );
};

export default SearchPage;
