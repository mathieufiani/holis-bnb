import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import PriceChange from '../../components/PriceChange/PriceChange';
import { firstLetterUppercase } from '../../utils/utils';
import { Location, Category } from '../../type';
import Loader from '../../components/Loader/Loader';
import './DisplayLocation.css';

type DisplayLocationPageProps = {};

const DisplayLocationPage: React.FC<DisplayLocationPageProps> = () => {
  const [location, setLocation] = useState<Location | undefined>(undefined);
  const [categories, setCategories] = useState<Category[] | undefined>(undefined);
  const [actualCategory, setActualCategory] = useState<Category | undefined>(undefined);

  /**
   * Fetching data on page loading
   */
  const id = useParams();
  useEffect(() => {
    loadLocation();
  }, []);

  useEffect(() => {
    if (location !== undefined) {
      setActualCategory(getCategory(location.categoryId));
    }
  }, [location]);

  /**
   * Loading location and categories from database to make data persistent on page reload
   */
  const loadLocation = async (): Promise<void> => {
    await axios.get('http://localhost:8000/categories').then((response: any) => {
      setCategories(response.data);
    });
    await axios.get(`http://localhost:8000/locations/${id.id}`).then((response) => {
      setLocation(response.data);
    });
  };

  /**
   * Mapping location's categoryId to the corresponding category.
   * @param categoryId
   */
  const getCategory = (categoryId: number): Category | undefined => {
    if (categories !== undefined) {
      const category: Category | undefined = categories.find(
        (category: Category) => category.id === categoryId
      );
      return category;
    }
    return undefined;
  };

  /**
   * Make page reactive to price change
   * @param updatedLocation
   */
  const handleUpdate = (updatedLocation: Location) => {
    setLocation(updatedLocation);
  };

  return (
    <>
      {location && actualCategory ? (
        <div>
          <img className={'display-location__img'} src={location.picture} alt="img" />
          <div className={'display-location__body'}>
            <div>
              <h1>{location.title}</h1>
              <h2>{firstLetterUppercase(actualCategory.propertyType)}</h2>
              <p>{actualCategory.description}</p>
              <span>{location.price} € per night</span>
            </div>
            <div className={'pb-6 md:pb-0'}>
              <PriceChange id={id.id as string} updateChange={handleUpdate} />
            </div>
          </div>
        </div>
      ) : (
        <div className={'display-location__loader'}>
          <Loader />
        </div>
      )}
    </>
  );
};

export default DisplayLocationPage;
