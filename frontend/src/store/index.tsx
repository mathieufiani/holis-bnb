import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { LocationsReducer } from './LocationsReducer';

export const store = createStore(
  combineReducers({
    searchName: LocationsReducer
  }),
  composeWithDevTools(applyMiddleware(thunk))
);
