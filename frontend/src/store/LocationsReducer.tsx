export const FILTER_LOCATIONS = 'FILTER_LOCATIONS';

type InitialState = {
  searchName: string;
};

const initialState: InitialState = {
  searchName: ''
};

export const LocationsReducer = (state: InitialState = initialState, action: any) => {
  switch (action.type) {
    case FILTER_LOCATIONS:
      return { searchName: action.payload };
    default:
      return state;
  }
};
