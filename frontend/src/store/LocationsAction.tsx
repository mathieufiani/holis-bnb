import { FILTER_LOCATIONS } from './LocationsReducer';

export const setSearchName = (value: string) => ({
  type: FILTER_LOCATIONS,
  payload: value
});
