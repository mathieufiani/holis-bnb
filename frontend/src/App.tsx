import React from 'react';
import SearchPage from './pages/Search/Search';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Layout from './layouts/Layout';
import DisplayLocation from './pages/DisplayLocation/DisplayLocation';
import { Provider } from 'react-redux';
import { store } from './store';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route element={<Layout />}>
            <Route index element={<SearchPage />} />
            <Route path={'/:id'} element={<DisplayLocation />} />
          </Route>
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
