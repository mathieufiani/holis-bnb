import React from 'react';
import { Location } from '../../type';
import { Link } from 'react-router-dom';
import './Card.css';

type CardProps = {
  location: Location;
};

const Card: React.FC<CardProps> = (props: CardProps) => {
  const { location } = props;
  const { title, picture, description, price, id }: Location = location;
  return (
    <div className="card">
      <Link to={`/${id}`}>
        <img className="card__img" src={picture} alt="location" />
      </Link>
      <div className={'p-5'}>
        <Link to={`/${id}`}>
          <h2 className={'card__title'}>{title}</h2>
        </Link>
        <p>{description.length > 50 ? description.slice(0, 50) + '...' : description}</p>
        <span>{price} €</span>
      </div>
    </div>
  );
};

export default Card;
