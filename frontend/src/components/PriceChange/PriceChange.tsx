import React, { useState } from 'react';
import axios from 'axios';
import { Location } from '../../type';
import './PriceChange.css';

type PriceChangeProps = {
  id: string;
  // eslint-disable-next-line no-unused-vars
  updateChange: (updatedLocation: Location) => void;
};

const PriceChange: React.FC<PriceChangeProps> = (props) => {
  const [newPrice, setNewPrice] = useState<string | undefined>();
  const { id, updateChange } = props;

  /**
   * If price is not empty, sending PATCH Request.
   */
  const submitUpdate = async () => {
    if (newPrice !== undefined && newPrice !== '') {
      await axios
        .patch(`http://localhost:8000/locations/${id}`, {
          price: { price: parseInt(newPrice as string) as number }
        })
        .then((response) => updateChange(response.data));
    }
  };

  /**
   * Forbid to enter characters that are not numbers.
   * @param e event
   */
  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value.replace(/\D/g, '');
    setNewPrice(value);
  };
  return (
    <div className="price-change">
      <div>
        <h4>Update Price of a night : </h4>
        <input
          type="update"
          className="price-change__input"
          placeholder="Enter new price"
          onChange={handleInput}
          value={newPrice}
          required
        />
      </div>
      <div>
        <button type="submit" className="price-change__button" onClick={() => submitUpdate()}>
          Update
        </button>
      </div>
    </div>
  );
};

export default PriceChange;
