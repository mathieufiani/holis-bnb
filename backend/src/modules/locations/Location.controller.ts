import { Body, Controller, Get, Logger, Param, Patch } from '@nestjs/common';
import { LocationService } from './Location.service';
import { UpdatePriceDto } from './Location.dto';

@Controller('locations')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  private readonly logger = new Logger();
  /** List all locations in database with this endpoint */
  @Get()
  async getLocations() {
    Logger.log('Executing GET Request');
    return await this.locationService.getLocations();
  }

  @Get('/:id')
  async getLocationById(@Param('id') id: number) {
    Logger.log('Executing GET Request');
    return await this.locationService.getLocationById(id);
  }

  @Patch('/:id')
  async updatePriceById(
    @Param('id') id: number,
    @Body('price') updateDto: UpdatePriceDto,
  ) {
    Logger.log('Executing PATCH request');
    const { price } = updateDto;
    return await this.locationService.updatePriceById(id, price);
  }
}
