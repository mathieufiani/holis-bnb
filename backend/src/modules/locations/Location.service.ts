import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Location } from './Location.entity';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location)
    private readonly locationRepository: Repository<Location>,
  ) {}

  async getLocations() {
    return await this.locationRepository.find();
  }

  async getLocationById(id: number): Promise<Location> {
    return await this.locationRepository.findOne({ where: { id } });
  }

  async updatePriceById(id: number, price: number): Promise<Location> {
    const location = await this.getLocationById(id);
    location.price = price;
    await this.locationRepository.save(location);
    return location;
  }
}
